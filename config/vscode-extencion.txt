PS C:\Users\jesus.reyess> code --list-extensions | % { "code --install-extension $_" }
code --install-extension aaron-bond.better-comments
code --install-extension CoenraadS.bracket-pair-colorizer-2
code --install-extension dracula-theme.theme-dracula
code --install-extension eamodio.gitlens
code --install-extension esbenp.prettier-vscode
code --install-extension felixfbecker.php-intellisense
code --install-extension kisstkondoros.vscode-codemetrics
code --install-extension kokororin.vscode-phpfmt
code --install-extension ms-vscode.vscode-typescript-tslint-plugin
code --install-extension neilbrayfield.php-docblocker
code --install-extension SonarSource.sonarlint-vscode
code --install-extension VisualStudioExptTeam.vscodeintellicode
code --install-extension vscode-icons-team.vscode-icons
code --install-extension Vtrois.gitmoji-vscode
code --install-extension WakaTime.vscode-wakatime
code --install-extension wayou.vscode-todo-highlight
code --install-extension yzhang.markdown-all-in-one